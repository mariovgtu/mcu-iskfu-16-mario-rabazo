/*
 * laboratory1.c
 *
 * Created: 2019-03-25 14:00:34
 * Author : 20180345
 */ 

#define F_CPU 16000000

#include <avr/io.h>
#include <util/delay.h>


int main(void)
{
    /* Replace with your application code */
	DDRA = 0b11111111;
	int a = 0;
	DDRC = 255;

    while (1)
    {
		if(a < 8){
			
			PORTA = (1<<a);
			a = a + 1;
			_delay_ms(250);
			
		}else{
			a = 1;
			while(a<8){
				PORTA = (128>>a);
				a = a + 1;
				_delay_ms(250); 
			}
			a = 1;
			
		}
		
		
    }
	return 0;
}

