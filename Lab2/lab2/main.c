/*
 * lab2.c
 *
 * Created: 2019-04-08 14:37:23
 * Author : 20180345
 */ 

#include <avr/io.h>
#include <util/delay.h>

void timer0_init()
{
	
	TCCR0 |= (1<<WGM00)|(1<<WGM01)|(1<<COM01)|(1 << CS00);
	DDRB|=(1<<PB3);
}

void SetPWMOutput(uint8_t duty)
{
	OCR0=duty;
}


int main(void)
{
	uint8_t brightness=0;
	
	timer0_init();
	
	while(1)
	{
		for(brightness=0;brightness<255;brightness++){
			SetPWMOutput(brightness);
			_delay_loop_2(3200);
		}
		
	}
}